﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SmartDemo.Application.Features.Books.Commands;
using SmartDemo.Application.Features.Books.Commands.CreateBook;
using SmartDemo.Application.Features.Books.Commands.DeleteBook;
using SmartDemo.Application.Features.Books.Commands.UpdateBook;
using SmartDemo.Application.Features.Books.Queries.GetAllBooks;
using SmartDemo.Application.Features.Books.Queries.GetBookById;
using SmartDemo.Application.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SmartDemo.Web.Api.Controllers.v1
{
    [ApiVersion("1.0")]
    public class BookController : BaseApiController
    {
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetAllBooksParameter filter)
        {
          
            return Ok(await Mediator.Send(new GetAllBooksQuery() { PageSize = filter.PageSize, PageNumber = filter.PageNumber  }));
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetBookByIdQuery { Id = id }));
        }

        // POST api/<controller>
        [HttpPost]
        [Authorize("create:books")]
        public async Task<IActionResult> Post(CreateBookCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        [Authorize("update:books")]
        public async Task<IActionResult> Put(int id, UpdateBookCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Authorize("delete:books")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteBookCommand { Id = id }));
        }
    }
}
