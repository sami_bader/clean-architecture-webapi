﻿using SmartDemo.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartDemo.Domain.Entities
{
    public class Book : AuditableBaseEntity
    {
        public string Title { get; set; }
        public string ISBN { get; set; }
        public string Summary { get; set; }
        public decimal Price { get; set; }
    }
}
