﻿using SmartDemo.Application.Interfaces.Repositories;
using SmartDemo.Domain.Entities;
using SmartDemo.Infrastructure.Persistence.Contexts;
using SmartDemo.Infrastructure.Persistence.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SmartDemo.Infrastructure.Persistence.Repositories
{
    public class BookRepositoryAsync : GenericRepositoryAsync<Book>, IBookRepositoryAsync
    {
        private readonly DbSet<Book> _books;

        public BookRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _books = dbContext.Set<Book>();
        }

        public Task<bool> IsUniqueISBNAsync(string isbn)
        {
            return _books
                .AllAsync(p => p.ISBN != isbn);
        }
    }
}
