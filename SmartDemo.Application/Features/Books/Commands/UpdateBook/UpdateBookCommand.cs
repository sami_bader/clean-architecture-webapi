﻿using SmartDemo.Application.Exceptions;
using SmartDemo.Application.Interfaces.Repositories;
using SmartDemo.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartDemo.Application.Features.Books.Commands.UpdateBook
{
    public class UpdateBookCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public decimal Price { get; set; }
        public class UpdateBookCommandHandler : IRequestHandler<UpdateBookCommand, Response<int>>
        {
            private readonly IBookRepositoryAsync _bookRepository;
            public UpdateBookCommandHandler(IBookRepositoryAsync bookRepository)
            {
                _bookRepository = bookRepository;
            }
            public async Task<Response<int>> Handle(UpdateBookCommand command, CancellationToken cancellationToken)
            {
                var book = await _bookRepository.GetByIdAsync(command.Id);

                if (book == null)
                {
                    throw new ApiException($"Book Not Found.");
                }
                else
                {
                    book.Title = command.Title;
                    book.Price = command.Price;
                    book.Summary = command.Summary;
                    await _bookRepository.UpdateAsync(book);
                    return new Response<int>(book.Id);
                }
            }
        }
    }
}
