﻿using SmartDemo.Application.Interfaces.Repositories;
using SmartDemo.Domain.Entities;
using FluentValidation;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmartDemo.Application.Features.Books.Commands.CreateBook
{
    public class CreateBookCommandValidator : AbstractValidator<CreateBookCommand>
    {
        private readonly IBookRepositoryAsync bookRepository;

        public CreateBookCommandValidator(IBookRepositoryAsync bookRepository)
        {
            this.bookRepository = bookRepository;

            RuleFor(p => p.ISBN)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MustAsync(IsUniqueISBN).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.Title)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(100).WithMessage("{PropertyName} must not exceed 100 characters.");
                
        }

        private async Task<bool> IsUniqueISBN(string isbn, CancellationToken cancellationToken)
        {
            return await bookRepository.IsUniqueISBNAsync(isbn);
        }
    }
}
