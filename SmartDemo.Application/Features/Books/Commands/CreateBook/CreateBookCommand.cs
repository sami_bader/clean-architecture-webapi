﻿using SmartDemo.Application.Interfaces.Repositories;
using SmartDemo.Application.Wrappers;
using AutoMapper;
using SmartDemo.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace SmartDemo.Application.Features.Books.Commands.CreateBook
{
    public partial class CreateBookCommand : IRequest<Response<int>>
    {
        public string Title { get; set; }
        public string ISBN { get; set; }
        public string Summary { get; set; }
        public decimal Price { get; set; }
    }
    public class CreateProductCommandHandler : IRequestHandler<CreateBookCommand, Response<int>>
    {
        private readonly IBookRepositoryAsync _bookRepository;
        private readonly IMapper _mapper;
        public CreateProductCommandHandler(IBookRepositoryAsync bookRepository, IMapper mapper)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateBookCommand request, CancellationToken cancellationToken)
        {
            var book = _mapper.Map<Book>(request);
            await _bookRepository.AddAsync(book);
            return new Response<int>(book.Id);
        }
    }
}
