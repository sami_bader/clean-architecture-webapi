﻿using SmartDemo.Application.Features.Books.Commands.CreateBook;
using SmartDemo.Application.Features.Books.Queries.GetAllBooks;
using AutoMapper;
using SmartDemo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartDemo.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Book, GetAllBooksViewModel>().ReverseMap();
            CreateMap<CreateBookCommand, Book>();
            CreateMap<GetAllBooksQuery, GetAllBooksParameter>();
        }
    }
}
