﻿using SmartDemo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartDemo.Application.Interfaces.Repositories
{
    public interface IBookRepositoryAsync : IGenericRepositoryAsync<Book>
    {
        Task<bool> IsUniqueISBNAsync(string isbn);
    }
}
