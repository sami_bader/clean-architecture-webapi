﻿using SmartDemo.Application.DTOs.Account;
using SmartDemo.Application.Wrappers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartDemo.Application.Interfaces
{
    public interface IAccountService
    {
        Task<Response<AuthenticationResponse>> AuthenticateAsync(AuthenticationRequest request);
        Task<Response<object>> GetUserInfoByTokenAsync(string accessToken);
    }
}
