﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SmartDemo.Application.DTOs.Account
{
    public class AuthenticationResponse
    {
        public string TokenId { get; set; }
        public string TokenType { get; set; }
        public string AccessToken { get; set; }
        [JsonIgnore]
        public string RefreshToken { get; set; }
        public int ExpiresIn { get; set; }
    }
}
