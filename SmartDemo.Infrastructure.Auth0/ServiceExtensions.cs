﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using SmartDemo.Application.Wrappers;
using SmartDemo.Infrastructure.Auth0.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Auth0.AuthenticationApi;
using SmartDemo.Application.Interfaces;
using SmartDemo.Infrastructure.Auth0.Services;

namespace SmartDemo.Infrastructure.Auth0
{
    public static class ServiceExtensions
    {
        public static void AddAuth0Infrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var domain = $"https://{configuration["Auth0:Domain"]}/";
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = domain;
                    options.Audience = configuration["Auth0:Audience"];
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = c =>
                        {
                            c.NoResult();
                            c.Response.StatusCode = 500;
                            c.Response.ContentType = "text/plain";
                            return c.Response.WriteAsync(c.Exception.ToString());
                        },
                        OnChallenge = context =>
                        {
                            context.HandleResponse();
                            context.Response.StatusCode = 401;
                            context.Response.ContentType = "application/json";
                            var result = JsonConvert.SerializeObject(new Response<string>("You are not Authorized"));
                            return context.Response.WriteAsync(result);
                        },
                        OnForbidden = context =>
                        {
                            context.Response.StatusCode = 403;
                            context.Response.ContentType = "application/json";
                            var result = JsonConvert.SerializeObject(new Response<string>("You are not authorized to access this resource"));
                            return context.Response.WriteAsync(result);
                        },
                        OnTokenValidated = context =>
                        {
                            // Grab the raw value of the token, and store it as a claim so we can retrieve it again later in the request pipeline
                            // Have a look at the ValuesController.UserInformation() method to see how to retrieve it and use it to retrieve the
                            // user's information from the /userinfo endpoint
                            if (context.SecurityToken is JwtSecurityToken token)
                            {
                                if (context.Principal.Identity is ClaimsIdentity identity)
                                {
                                    identity.AddClaim(new Claim("access_token", token.RawData));
                                }
                            }

                            return Task.CompletedTask;
                        }
                    };
                });
            
            services.AddAuthorization(options =>
            {
                options.AddPolicy("read:messages", policy => policy.Requirements.Add(new HasScopeRequirement("read:messages", domain)));
                options.AddPolicy("read:books", policy => policy.Requirements.Add(new HasScopeRequirement("read:books", domain)));
                options.AddPolicy("create:books", policy => policy.Requirements.Add(new HasScopeRequirement("create:books", domain)));
                options.AddPolicy("update:books", policy => policy.Requirements.Add(new HasScopeRequirement("update:books", domain)));
                options.AddPolicy("delete:books", policy => policy.Requirements.Add(new HasScopeRequirement("delete:books", domain)));
            });

            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

            services.AddSingleton(x =>
                new AuthenticationApiClient(new Uri($"https://{configuration["Auth0:Domain"]}/")));

            services.AddTransient<IAccountService, AccountService>();

        }
    }
}
