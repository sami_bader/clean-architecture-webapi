﻿using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Microsoft.Extensions.Configuration;
using SmartDemo.Application.DTOs.Account;
using SmartDemo.Application.Interfaces;
using SmartDemo.Application.Wrappers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartDemo.Infrastructure.Auth0.Services
{
    public class AccountService : IAccountService
    {
        private readonly AuthenticationApiClient _client;
        private readonly IConfiguration _configuration;
        public AccountService(AuthenticationApiClient client, IConfiguration configuration)
        {
            _client = client;
            _configuration = configuration;
        }
        public async Task<Response<AuthenticationResponse>> AuthenticateAsync(AuthenticationRequest request)
        {
            var result = await _client.GetTokenAsync(new ResourceOwnerTokenRequest()
            {
                ClientId = _configuration["Auth0:ClientId"],
                ClientSecret = _configuration["Auth0:ClientSecret"],
                Audience = _configuration["Auth0:Audience"],
                Scope = "openid",
                Username = request.Email,
                Password = request.Password
            });
            AuthenticationResponse response = new AuthenticationResponse();
            response.AccessToken = result.AccessToken;
            response.ExpiresIn = result.ExpiresIn;
            response.RefreshToken = result.RefreshToken;
            response.TokenId = result.IdToken;
            response.TokenType = result.TokenType;
            return new Response<AuthenticationResponse>(response, $"Authenticated Successful");
        }

        public async Task<Response<object>> GetUserInfoByTokenAsync(string accessToken)
        {
            var result = await _client.GetUserInfoAsync(accessToken);
            return new Response<object>(result, $"User Information");
        }
    }
}
